<?php

include_once drupal_get_path('module','feeds') . '/plugins/FeedsSimplePieParser.inc';

/**
 * Class definition for Common Syndication Parser.
 *
 * Parses RSS and Atom feeds.
 * FeedsSimplePieParserKirby
 */
class FeedsSimplePiePlusParser extends FeedsSimplePieParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = parent::parse($source,$fetcher_result) ;
    $source_config = $source->getConfigFor($this);
    if ( isset($result->items) ) {
      foreach ( $result->items as $key => $item ) {
        foreach ( $item['enclosures'] as $enclosure ) {
          $mimeType = $enclosure->getMIMEType();
          $mimeType = explode('/', $mimeType ) ;
          switch ( $mimeType[0] ) {
            case 'audio':
              $result->items[$key]['enclosures_audio'][] = $enclosure ;
            break ;
            case 'video':
              $result->items[$key]['enclosures_video'][] = $enclosure ;
            break ;
            case 'image':
              $result->items[$key]['enclosures_image'][] = $enclosure ;
            break ;
          }
        }
        if ( !empty($item['description']) ) {
          $iimages = self::parseInlineImages($item['description']) ;
          if ( $iimages ) {
            $result->items[$key]['inline_images'] = $iimages ;
            $result->items[$key]['enclosures'] = array_merge($result->items[$key]['enclosures']) ;
            if ( $this->config['remove_inline_images'] == true ) {
              $result->items[$key]['description'] = preg_replace("/<img[^>]+\>/i", "", $result->items[$key]['description']); ;
            }
          }
        }
      }
    }

    // Release parser.
    unset($parser);
    // Set error reporting back to its previous value.
    error_reporting($level);
    return $result;
  }


  /**
   * Define default configuration.
   */
  public function configDefaults() {
    return array(
        'remove_inline_images' => FALSE,
    );
  }

  /**
   * Build configuration form.
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['remove_inline_images'] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove inline images'),
        '#return_value' => true,
        '#default_value' => $this->config['remove_inline_images'],
    );
    return $form;
  }


  protected function parseInlineImages($body) {
    $doc = DOMDocument::loadHTML($body) ;
    $images = $doc->getElementsByTagName('img');
  foreach ( $images as $img ) {
    $simplePie = new SimplePie_Enclosure( $img->getAttribute('src'), NULL, NULL, NULL,  NULL, NULL, NULL, NULL, NULL,  NULL,
                                          $img->getAttribute('alt'), NULL,  NULL,  NULL, NULL, $img->getAttribute('height'),
                                          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $img->getAttribute('title'), $img->getAttribute('width') );
    $iimages[] = new FeedsSimplePieEnclosure($simplePie);
  }
  return $iimages ;
  }

  /**
   * Allow extension of FeedsSimplePie item parsing.
   */
  protected function parseExtensions(&$item, $simplepie_item) {}

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    return array(
      'enclosures_audio' => array(
        'name' => t('Audio Enclosures'),
        'description' => t('An array of enclosures attached to the feed item.'),
      ),
      'enclosures_video' => array(
          'name' => t('Video Enclosures'),
          'description' => t('An array of enclosures attached to the feed item.'),
      ),
      'enclosures_image' => array(
        'name' => t('Image Enclosures'),
        'description' => t('An array of enclosures attached to the feed item.'),
      ),
      'inline_images' => array(
        'name' => t('Inline Images'),
        'description' => t('An array of enclosures attached to the feed item.'),
      ),
     ) + parent::getMappingSources();
  }
}
